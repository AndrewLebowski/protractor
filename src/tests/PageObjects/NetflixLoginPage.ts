import { $, ElementFinder } from 'protractor';

class LoginPage {

  get loginInput():ElementFinder{
    return $('#id_userLoginId')
  }

  get passwordInput():ElementFinder{
    return $('#id_password');
  }

  get submitButton(): ElementFinder{
    return $('.login-button');
  }

  get seePlanButton():ElementFinder{
    return $('[type="button"]');
  }

  get secondStep(): ElementFinder{
    return  $('.stepIndicator b:first-child');
  }
  
}

export default new LoginPage();