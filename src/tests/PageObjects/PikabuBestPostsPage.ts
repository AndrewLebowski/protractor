import { $, $$, ElementFinder, ElementArrayFinder } from "protractor";





class BestPostsPage {


    get bestPostsTab():ElementFinder {
        return $('a[href="/best"]');
    }

    get posts():ElementArrayFinder {
        return $$('.story');
    }

    get dateInput():ElementFinder {
        return $('.feed-panel__calendar');
    }

    get calendarWidget():ElementFinder {
        return $('.calendar');
    }

    get calendarFromDateInput():ElementFinder {
        return $('.calendar [data-type="from"]');
    }

    get calendarToDateInput():ElementFinder {
        return $('.calendar [data-type="to"]');
    }
    
    get showPostsButton():ElementFinder {
        return $('.button ');
    }

    get loadingAnimation():ElementFinder {
        return $('[data-type="animation"]');
    }
}

export default new BestPostsPage();