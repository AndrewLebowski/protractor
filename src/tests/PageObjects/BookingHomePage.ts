import { browser, $, $$, ElementFinder, ElementArrayFinder, element } from "protractor";
import Helpers from '../Helpers/HelpersBooking'




class HomePage {

    get selectPeopleInput():ElementFinder { 
        return $('#xp__guests__toggle')
    }

    get addChildButton():ElementFinder {
        return $('.sb-group-children .bui-stepper__add-button')
    } 

    get childrenInput():ElementFinder {
        return $('#group_children')
    }

    get childrenInputsArray():ElementArrayFinder {
        return $$('.sb-group__children__field select')
    }

    get subtractChildButton():ElementFinder {
        return $('.sb-group-children .bui-stepper__subtract-button')
    }

    get languagesButton():ElementFinder {
        return $('.uc_language')
    }

    get englishLanguageButton():ElementFinder {
        return $('.lang_en-us')
    }

    get cityInput():ElementFinder {
        return $('#ss')
    }

    get cityInputListItemsArray():ElementArrayFinder {
        return $$('ul[role="listbox"] li')
    }

    get addMonthButton():ElementFinder {
        return $('.bui-calendar__control--next')
    }

    get datesArray():ElementArrayFinder {
        return $$('.bui-calendar__content td[data-date]')
    }

    get selectedDatesArray():ElementArrayFinder {
        return $$('.bui-calendar__date--selected')
    }

    get addAdultButton():ElementFinder {
        return $('.sb-group__field-adults .bui-stepper__add-button')
    }

    get addRoomButton():ElementFinder {
        return $('.sb-group__field-rooms .bui-stepper__add-button')
    }

    get adultsInput():ElementFinder {
        return $('#group_adults')
    }

    get roomsInput():ElementFinder {
        return $('#no_rooms')
    }

    get onMapCheckpoint():ElementFinder {
        return $('[for="sb_results_on_map"]')
    }
    
    get submitButton():ElementFinder {
        return $('.accommodation [type="submit"]')
    }




    open():void {
        browser.get('https://www.booking.com/')
    }

    selectChildrenAge():void {
        this.childrenInputsArray.each( item => {
            item.click()
            const randomAge:number = Helpers.getRandomInt(0, 17)
            item.$(`[value="${randomAge}"]`).click()
        })
    }

    async getChildAges() {
        const array:ElementArrayFinder = this.childrenInputsArray.$$('option[selected]')
        const arrayOfResults:string[] = await array.map( async item => {
            const value:string = await item.getAttribute('value')   
            return value    
        })     
        return arrayOfResults
    }

    chooseRandomDate() {
        //random month
        const randomMonth:number = Helpers.getRandomInt(1, 14)
        const addMonth:ElementFinder = this.addMonthButton
        Helpers.clickOnElementNTimes( addMonth, randomMonth)

        //random dates
        const randomFirstDateIndex:number = Helpers.getRandomInt(1, 14)
        const randomLastDateIndex:number = Helpers.getRandomInt(1, 14)
    
        this.datesArray.get( randomFirstDateIndex ).click()
        this.datesArray.get( randomFirstDateIndex + randomLastDateIndex ).click()   
    }

    async getMiliseconds() {
        let firstDateSeconds:string
        let lastDateSeconds:string

            await this.selectedDatesArray.each( async item => {
                const currentDate:string = await item.getAttribute('data-date')
                const currentDateSecondsValue:number = new Date(currentDate).getTime()
                const currentDateSeconds:string = currentDateSecondsValue.toString()
                if (!firstDateSeconds) {
                    firstDateSeconds = currentDateSeconds;
                } else {
                    lastDateSeconds = currentDateSeconds;
                }
            })

        return [firstDateSeconds, lastDateSeconds]
    }

    async chooseCity(value:string ) {
        this.cityInput.sendKeys(value)
        browser.sleep(3000)
        const length = await this.cityInputListItemsArray.count()
        const randomIndex = Helpers.getRandomInt(1, length)
        const randomItem = this.cityInputListItemsArray.get(randomIndex)
        randomItem.click()
    }


    async checkIfAgesAreSelected() {  
        const arrayOfResults:boolean[] = []    
        const selectedArray:ElementFinder[] = await this.childrenInputsArray.$$('option[selected]')
        await selectedArray.forEach( async element => {   
            const elementClass:string = await element.getAttribute('class')
            if(elementClass !== 'sb_child_ages_empty_zero') {
                arrayOfResults.push(true)
            } else {
                arrayOfResults.push(false)
            }
                
        })
        const areAgesSelected:boolean = !arrayOfResults.includes(false)
        return areAgesSelected   
    }

    async clearChildrenValue() {
        while(await this.subtractChildButton.isEnabled()) {
            this.subtractChildButton.click();
        }
        
    }

}

export default new HomePage();