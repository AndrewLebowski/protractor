import { browser, $, $$, ElementFinder, ElementArrayFinder, element } from "protractor";


class HotelsPage {

    get cityInput():ElementFinder {
        return $('#ss')
    }

    get calendar():ElementFinder {
        return $('.sb-date-field__wrapper')
    }

    get firstDateSeconds():ElementFinder {
        return $('.c2-day-s-first-in-range')
    }

    get lastDateSeconds():ElementFinder {
        return $('.c2-day-s-last-in-range')
    }

    get adultsInput():ElementFinder {
        return $('#group_adults option[selected]')
    }

    get childrenInput():ElementFinder {
        return $('#group_children option[selected]')
    }

    get roomsInput():ElementFinder {
        return $('#no_rooms option[selected]')
    }

    get childrenSelectedInputsArray():ElementArrayFinder {
        return $$('.sb-group__children__field option[selected]')
    }

    get starsCheckboxesArray():ElementArrayFinder {
        return $$('[data-id="filter_class"] a')
    }

    get scoresCheckboxesArray():ElementArrayFinder {
        return $$('#filter_review a ');
    }

    get hotelsArray():ElementArrayFinder {
        return $$('#hotellist_inner .sr_item')
    }

    

    async getChildAges() {
        const arrayOfResults:string[] = await this.childrenSelectedInputsArray.map( async item => {
            const value:string = await item.getAttribute('value')
            return value
        })     
        return arrayOfResults
    }

}

export default new HotelsPage
