import { browser, element, by, $, $$, ElementFinder, ElementArrayFinder } from "protractor";





class HomePage {

   get loginForm():ElementFinder {
       return $('.sidebar-auth')
   }

   get commentOfTheDayWidget():ElementFinder {
       return element(by.cssContainingText('.sidebar-block__header', 'Комментарий дня'));
   }

   get dateInput():ElementFinder {
       return $('.feed-panel__calendar');
   }

   get revisedPostsOption():ElementFinder {
       return $('.sidebar-toggle__action');
   }

   get revisedPostsOptionList():ElementFinder {
       return $('.popup__content');
   }

   get posts():ElementArrayFinder {
       return $$('.story');
   }

   get collapseOption():ElementFinder {
       return element(by.cssContainingText('.popup__content p', 'Сворачивать'));
   }


    open():void {
        browser.get('https://pikabu.ru/');
    }

}

export default new HomePage();