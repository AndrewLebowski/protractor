import {$, $$, browser, ElementFinder, ElementArrayFinder} from 'protractor';

class HomePage {


  
  /**
   *
   *
   * @readonly
   * @type {ElementFinder} 
   * @memberof HomePage 
   */ 



  get signInButton(): ElementFinder{
    return $('.authLinks');
  }

  get faqList(): ElementArrayFinder{
    return $$('.faq-list-item');
  }

  get tryFreeButton(): ElementFinder{
    return $('.our-story-card-footer-text [role="link"]');
  }

  openHomePage(): void {
    browser.get('https://www.netflix.com');
  }
}

export default new HomePage();