import { Config } from 'protractor';

export let config: Config = {

    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ["./specs/pikabu.spec.js"],
    jasmineNodeOpts: {
        defaultTimeoutInterval: 3000000
    },
}