import HomePage from '../PageObjects/PikabuHomePage'
import BestPostsPage from '../PageObjects/PikabuBestPostsPage'
import { browser, ElementFinder } from 'protractor';








describe('*** some description ***', () => {


    beforeAll( () => browser.waitForAngularEnabled(false) );

    beforeEach( () => HomePage.open() );



    it('Correct element on main page', () => {

        
        //tab "Hot" is opened by default
        expect( browser.getTitle() ).toBe('Горячее');



        //login form is displayed on the main page
        expect( HomePage.loginForm.isDisplayed() ).toBeTruthy();



        //widget "Comment of the day" is displayed
        expect( HomePage.commentOfTheDayWidget.isDisplayed() ).toBeTruthy();



        //date is not choosen
        expect( HomePage.dateInput.getText() ).toBe('выбрать дату');


    });



    it('In the "Best" tab, posts are ranked in descending order', async () => {


        //tab "Best posts" is opened
        BestPostsPage.bestPostsTab.click();
        browser.sleep(2000)
        const title = await browser.getTitle()
        expect( title ).toBe('Лучшее');



        //posts are in correct order
        let currentRating:number = 999999;
        for( let i = 0; i < 10; i++ )    //i make comparison of first 10 posts because older posts change raiting very dynamicaly 
        {
            const post = BestPostsPage.posts.get(i)
            const rating:number = Number( await post.getAttribute('data-rating') );
            const isOrderCorrect:boolean = currentRating >= rating
            expect( isOrderCorrect ).toBeTruthy();
            currentRating = rating;

        }

    });



    it('In the "Best" tab, date filtering works correct', async () => {


        //specify dates here in this format: "day/month/year"
        const firstDateInRange:string = '14/07/2019';
        const lastDateInRange:string = '21/07/2019';



        //tab "Best posts" is opened
        BestPostsPage.bestPostsTab.click();
        const title = await browser.getTitle();
        expect( title ).toBe('Лучшее');



        //correct filter by default
        const dateInputText = await HomePage.dateInput.getText();
        expect( dateInputText ).toBe('за сегодня');



        //calendar widget is opened
        BestPostsPage.dateInput.click();
        const isCalendarDisplayed = await BestPostsPage.calendarWidget.isDisplayed();
        expect( isCalendarDisplayed ).toBeTruthy();



        //button "Show posts" is displayed
        BestPostsPage.calendarFromDateInput.clear(); 
        BestPostsPage.calendarFromDateInput.sendKeys(firstDateInRange);   
        browser.sleep(2000);
        BestPostsPage.calendarToDateInput.click();
        BestPostsPage.calendarToDateInput.clear();
        BestPostsPage.calendarToDateInput.sendKeys(lastDateInRange);
        browser.sleep(2000);
        const isShowPostsButtonDisplayed = await BestPostsPage.showPostsButton.isDisplayed()
        expect( isShowPostsButtonDisplayed ).toBeTruthy();



        //loading animation is present
        BestPostsPage.showPostsButton.click();
        const isAnimationPresent = await BestPostsPage.loadingAnimation.isPresent()
        expect( isAnimationPresent ).toBeTruthy();
        browser.sleep(3000);


        
        //posts are ranked in descending order
        let currentRating:number = 999999;
        for( let i = 0; i < 10; i++ )    //i make comparison of first 10 posts because older posts change raiting very dynamicaly 
        {
            const post = BestPostsPage.posts.get(i)
            const rating:number = Number( await post.getAttribute('data-rating') );
            const isOrderCorrect:boolean = currentRating >= rating
            expect( isOrderCorrect ).toBeTruthy();
            currentRating = rating;

        }


        //selected posts are in range
        const firstSelectedDate:string = firstDateInRange.split('/').reverse().join('-');
        const lastSelectedDate:string = lastDateInRange.split('/').reverse().join('-');
        const firstDateSeconds:number = Date.parse(firstSelectedDate);
        const lastDateSeconds:number = Date.parse(lastSelectedDate);
        const postsArrayLength:number = await BestPostsPage.posts.count();
            for(let i = 0; i < postsArrayLength - 1; i++ )
            {
                const time:string = await BestPostsPage.posts.get(i).$('time').getAttribute('datetime');
                const seconds = Date.parse(time);                       // add or subtract one day for correct range                                                        
                const isDateInRange:boolean = seconds > (firstDateSeconds - 86400000)  && seconds < (lastDateSeconds + 86400000);
                expect( isDateInRange ).toBeTruthy();

            };


    });



    it('Option "Collapse revised posts" works correct', async () => {


        //correct option by default
        expect( HomePage.revisedPostsOption.getText() ).toBe('показывать');



        //list with options is opened
        HomePage.revisedPostsOption.click();
        expect( await HomePage.revisedPostsOptionList.isDisplayed() ).toBeTruthy();



        //open 3 posts in new windows
            for( let i = 0; i < 3; i++) 
            {
                const postTitle = HomePage.posts.get(i).$('.story__title');
                postTitle.click();        
                
            }
        const tabs = await browser.getAllWindowHandles();   
        expect( tabs.length ).toBe( 4 );
        await browser.switchTo().window(tabs[0]);



        //show revised posts
        browser.sleep(10000);
        HomePage.open();
            for( let i = 0; i < 3; i++) 
            {
                const postContent = await HomePage.posts.get(i).$('.story__content').isDisplayed();
                expect( postContent ).toBe( true );        
            }



        //collapse revised posts
        HomePage.revisedPostsOption.click();
        HomePage.collapseOption.click();
        HomePage.open();
            for( let i = 0; i < 3; i++) 
            {
                const postContent = await HomePage.posts.get(i).$('.story__content').isDisplayed();
                expect( postContent ).toBe( false );        
            }
        

    });


});

