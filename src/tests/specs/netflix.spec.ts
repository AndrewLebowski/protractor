import { browser, $$ } from 'protractor';
import HomePage from '../PageObjects/NetflixHomePage';
import LoginPage from '../PageObjects/NetflixLoginPage';



describe('Protractor Netflix', () => {
    

    beforeAll( () => {
      browser.waitForAngularEnabled(false);
      HomePage.openHomePage();
    });


    it('User can type correct input values ', async () => {
      
      const errorClass = 'nfTextField error hasText';
      const successClass = 'nfTextField hasText';
      

      HomePage.signInButton.click();
      LoginPage.loginInput.sendKeys('1234');
      LoginPage.passwordInput.sendKeys('123');
      LoginPage.submitButton.click();
      browser.sleep(1000);


      let classLoginInput = await LoginPage.loginInput.getAttribute('class');
      let classPasswordInput = await LoginPage.passwordInput.getAttribute('class');
      expect(classLoginInput).toBe(errorClass);
      expect(classPasswordInput).toBe(errorClass);
      

      HomePage.openHomePage();
      HomePage.signInButton.click();
      LoginPage.loginInput.sendKeys('12345');
      LoginPage.passwordInput.sendKeys('1235');

      classLoginInput = await LoginPage.loginInput.getAttribute('class');
      classPasswordInput = await LoginPage.passwordInput.getAttribute('class');


      expect(classLoginInput).toBe(successClass);
      expect(classPasswordInput).toBe(successClass);
    
    });
    


    it('User is able to see content of faq articles', () => {

        HomePage.openHomePage();
        const openedElementClass = 'faq-answer open';
        HomePage.faqList.each( async item => {
            item.click();
            browser.sleep(2000);
            const elementClass = await item.$('.faq-answer').getAttribute('class');
            expect(elementClass).toBe(openedElementClass);       
        });  

    });



    fit('User can be authorized after making choice of user plan', () => {

      HomePage.openHomePage();
      HomePage.tryFreeButton.click();
      LoginPage.seePlanButton.click();
      browser.sleep(4000)
  
      LoginPage.seePlanButton.click();

      HomePage.openHomePage();
      HomePage.tryFreeButton.click();

      const step = LoginPage.secondStep.getText();
      expect(step).toBe('2');
      
    });
    
  });