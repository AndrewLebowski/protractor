import { browser, ElementFinder, ElementArrayFinder } from 'protractor'
import HomePage from '../PageObjects/BookingHomePage'
import HotelsPage from '../PageObjects/BookingHotelsPage'
import Helpers from '../Helpers/HelpersBooking'





describe('*** some description ***', () => {

    beforeAll( () => browser.waitForAngularEnabled(false) )

    beforeEach( () => HomePage.open() )

    afterEach( () => {
        browser.manage().deleteAllCookies()
    })

    it('User is able to specify age of each child', async () => {


        //select random ages of children
        browser.sleep(2000)
        HomePage.selectPeopleInput.click()
        browser.sleep(2000)
        const randomInteger:number = Helpers.getRandomInt( 1, 10 )
        const addChild:ElementFinder = HomePage.addChildButton
        Helpers.clickOnElementNTimes( addChild, randomInteger )
        browser.sleep(2000)
        HomePage.selectChildrenAge()
        HomePage.selectPeopleInput.click()


        //children amount is equal to random integer
        const childrenAmount:string = await HomePage.childrenInput.getAttribute('value')
        expect(Number(childrenAmount)).toBe(randomInteger)
  
      
        // //inputs amount is equal to random integer
        const ageInputs:number = await HomePage.childrenInputsArray.count()
        expect(ageInputs).toBe(randomInteger)

    
        //ages are selected
        const areAgesSelected:boolean = await HomePage.checkIfAgesAreSelected();
        expect(areAgesSelected).toBeTruthy()
        

        HomePage.clearChildrenValue()

    });



    it('User is provided with the same search form at search results page', async () => {

  

        //choose certain language for further comparison
        browser.sleep(2000)
        HomePage.languagesButton.click()
        HomePage.englishLanguageButton.click();
        

        //random city
        browser.sleep(2000)
        HomePage.chooseCity('Rivne')
        const cityValueHomePage:string = await HomePage.cityInput.getAttribute('value')


        //randome date 
        browser.sleep(2000)
        HomePage.chooseRandomDate()
        const [ HomePageFirstDateSeconds, HomePageLastDateSeconds ]:string[] = await HomePage.getMiliseconds()
  
        
        //select random amount of people
        browser.sleep(2000)
        HomePage.selectPeopleInput.click() 

        const addAdultsClicks:number = Helpers.getRandomInt(1, 10)
        const addChildrenClicks:number = Helpers.getRandomInt(1, 10)
        const addRoomClicks:number = Helpers.getRandomInt(2, 7)

        Helpers.clickOnElementNTimes( HomePage.addChildButton, addChildrenClicks )
        Helpers.clickOnElementNTimes( HomePage.addAdultButton, addAdultsClicks )
        Helpers.clickOnElementNTimes( HomePage.addRoomButton, addRoomClicks )

        const adultsAmountHomePage:string = await HomePage.adultsInput.getAttribute('value')
        const childrenAmountHomePage:string = await HomePage.childrenInput.getAttribute('value')
        const roomsAmountHomePage:string = await HomePage.roomsInput.getAttribute('value')
       


        //select children ages
        browser.sleep(2000)
        HomePage.selectChildrenAge()
        HomePage.selectPeopleInput.click() 
        

        //prevent map occurring after submit
        browser.sleep(2000)
        if( await HomePage.onMapCheckpoint.isDisplayed() ) {
            HomePage.onMapCheckpoint.click()
        }

        
        //array of child ages
        const childrenAgesHomePage:string[] = await HomePage.getChildAges()

             
        browser.sleep(2000)
        HomePage.submitButton.click()
        browser.sleep(4000)


        // correct city
        const cityValueHotelsPage:string = await HotelsPage.cityInput.getAttribute('value')
        const isCityCorrect:boolean = cityValueHomePage.includes( cityValueHotelsPage )
        expect( isCityCorrect ).toBeTruthy()


        //correct date
        HotelsPage.calendar.click()
        const HotelsPageFirstDateSeconds:string = await HotelsPage.firstDateSeconds.getAttribute('data-id')
        const HotelsPageLastDateSeconds:string = await HotelsPage.lastDateSeconds.getAttribute('data-id')
        const isDatesCorrect:boolean = 
                HomePageFirstDateSeconds === HotelsPageFirstDateSeconds && 
                HomePageLastDateSeconds === HotelsPageLastDateSeconds
        expect( isDatesCorrect ).toBeTruthy()


        //correct adult amount 
        const adultsAmountHotelsPage:string = await HotelsPage.adultsInput.getAttribute('value')
        expect( adultsAmountHomePage ).toBe( adultsAmountHotelsPage )
       

        //correct child amount  
        const childrenAmountHotelsPage:string = await HotelsPage.childrenInput.getAttribute('value')
        expect( childrenAmountHomePage ).toBe( childrenAmountHotelsPage )
 
 
        //correct rooms amount   
        const roomsAmountHotelsPage:string = await HotelsPage.roomsInput.getAttribute('value')
        expect( roomsAmountHomePage ).toBe( roomsAmountHotelsPage )
 
         
        //is ages correct
        const childrenAgesHotelsPage:string[] = await HotelsPage.getChildAges()
        const areAgesCorrect:boolean = Helpers.checkArraysMatching( childrenAgesHomePage, childrenAgesHotelsPage )
        expect(areAgesCorrect).toBeTruthy();

    })

    it('Resulting search entries are taken based on filter', async () => {

        //choose certain language for further comparison
        browser.sleep(2000)
        HomePage.languagesButton.click()
        HomePage.englishLanguageButton.click()

        

        //random city
        browser.sleep(2000)
        HomePage.chooseCity('Lviv')

 
        //randome date 
        browser.sleep(2000)
        HomePage.chooseRandomDate()
    

        //clear remained values
        browser.sleep(2000)
        HomePage.selectPeopleInput.click()
        browser.sleep(2000)
        HomePage.clearChildrenValue()
        HomePage.selectPeopleInput.click()



        //prevent map occurring after submit  
        browser.sleep(2000)
        if( await HomePage.onMapCheckpoint.isDisplayed() ) {
            HomePage.onMapCheckpoint.click()
        }


        HomePage.submitButton.click()
        browser.sleep(4000)


        //comparison between chosen rating and rating of each hotel on the page
        const arrayOfStarsResults:boolean[] = []
        const starsCheckboxesLength:number = await HotelsPage.starsCheckboxesArray.count()
        for( let i = 0; i < starsCheckboxesLength; i++ ) 
        {
            const item:ElementFinder = await HotelsPage.starsCheckboxesArray.get(i)
            item.click()
            browser.sleep(4000)
            const starCheckboxValue:string = await item.getAttribute('data-value')
                if( starCheckboxValue === 'Unrated' ) 
                {
                    await HotelsPage.hotelsArray.each( async hotel => {
                        const unratedHotelValue:string = await hotel.getAttribute('data-class')
                        arrayOfStarsResults.push(unratedHotelValue === '0')
                    })
                } 
                else 
                {
                    await HotelsPage.hotelsArray.each( async hotel => {
                        let hotelStarsAmount:string = await hotel.getAttribute('data-class')
                            if( hotelStarsAmount === '0' ) 
                            {
                                const bookingStars:ElementFinder[] = await hotel.$$('span svg.-iconset-square_rating')
                                const bookingStarsAmount:string = await bookingStars.length.toString()
                                hotelStarsAmount = bookingStarsAmount
                            }
                        arrayOfStarsResults.push(hotelStarsAmount === starCheckboxValue || hotelStarsAmount === null)
                    })
                }
            
                item.click()         
        }
        const isStarsAmountCorrect:boolean = !arrayOfStarsResults.includes(false);  
        expect(isStarsAmountCorrect).toBeTruthy();
    

       //each hotel has rating corresponding to chosen
       const arrayOfScoresResults:boolean[] = []
       const scoresCheckboxesLength:number = await HotelsPage.scoresCheckboxesArray.count()
       for ( let i = 0; i < scoresCheckboxesLength; i++) 
       {
        const item:ElementFinder = await HotelsPage.scoresCheckboxesArray.get(i)
        item.click()
        browser.sleep(4000)
        const scoreCheckboxValue:string = await item.getAttribute('data-value')
            if(scoreCheckboxValue === '999') 
            {
                await HotelsPage.hotelsArray.each( async hotel => {
                    const isScoreDisplayed:boolean = await hotel.$('.sr-review-score').isPresent()
                    arrayOfScoresResults.push( !isScoreDisplayed )
               })
            }
            else 
            {
                await HotelsPage.hotelsArray.each( async hotel => {
                    const hotelScore:string = await hotel.getAttribute('data-score')
                    arrayOfScoresResults.push( Number(hotelScore) >= Number(scoreCheckboxValue) / 10 )
                }) 
            }
        item.click()
       }
       browser.sleep(3000)
       const isScoresAmountCorrect:boolean = !arrayOfScoresResults.includes(false)
       expect(isScoresAmountCorrect).toBeTruthy()


    })

});