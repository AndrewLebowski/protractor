import { ElementFinder } from "protractor";


class Helpers {

    getRandomInt(min: number, max: number) {
        return Math.floor(Math.random() * (max - min) + min)
    }

    clickOnElementNTimes(button: ElementFinder, randomNum: number) {
        for(let i = 0; i < randomNum; i++) {
            button.click()
        } 
    }

    checkArraysMatching(arr1:string[], arr2:string[]) {
        const a:string = arr1.sort().join(', ')
        const b:string = arr2.sort().join(', ')
        return a === b;
    }

}

export default new Helpers();