# Protractor project using TypeScript
Simple attemption to check Protector possibilities

In the case of a small performance of your system and 
Internet quality, it is recommended to increase the timeouts

### Prerequisites

- Node: v10 and later
- npm:  v6 and later
- Installed JDK on your local machine
- Protractor supports the two latest major versions of Chrome, Firefox, Safari, and IE

Here by default I use Chrome but u can set up another browser using [documentation](https://www.protractortest.org/#/browser-setup)

## Getting Started

### Install dependencies
In this project you don't need to use global modules.Run following to set up local:
```
npm install
```
### Open server
Run following command in separated command line/terminal window for starting selenium server:

```
npm run server
```
### Compilation
Use following command to compile TypeScript code into JS:

```
npm run compile
```
### Test
Finally run test:

```
npm test
```

